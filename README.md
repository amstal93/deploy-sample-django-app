# Deploy sample django app
This is practice project where i deploy [devops-sample-django-app](https://github.com/simbirsoft/devops-sample-django-app) using docker-compose with
* Django
* Postgres
* Nginx
* Prometheus
## Requirements
* Docker
## Usage
##### Download project and go to the services directory
```
cd ./services
```
##### Build and run containers
```
docker compose -f docker-compose.prod.yml up -d
```
##### Run django migrations
```
docker compose -f docker-compose.prod.yml exec web python manage.py migrate
```
##### Local urls
Django app http://localhost:8888 <br/> Prometheus server http://localhost:9090

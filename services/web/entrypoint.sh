#!/bin/sh

echo "Waiting for database..."
while ! nc -z $DJANGO_DB_HOST $DJANGO_DB_PORT; do
  sleep 0.1
done
echo "Database started"

python manage.py makemigrations
python manage.py migrate

exec "$@"